package gr.edu.telkav.ocr;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;

import gr.edu.telkav.ocr.view.screens.menu.MainActivity;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);




        final Button btnOpenPopup = (Button) findViewById(R.id.openpopup);
        final View popupView = getLayoutInflater().inflate(R.layout.view_popup, null);
        View anchor = findViewById(R.id.anchor);

        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);

        final View finalAnchor = anchor;
        btnOpenPopup.setOnClickListener(new Button.OnClickListener() {
            final Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);

            @Override
            public void onClick(View arg0) {
                popupWindow.showAsDropDown(finalAnchor, 50, -30);

                btnDismiss.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });
            }

        });


        //edo
        final Button btnOpenPopup2 = (Button) findViewById(R.id.openpopup2);
        final View popupView2 = getLayoutInflater().inflate(R.layout.view_contact, null);
        anchor = findViewById(R.id.anchor);

        final PopupWindow popupWindow2 = new PopupWindow(
                popupView2,
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);

        View finalAnchor1 = anchor;
        final View finalAnchor2 = finalAnchor1;
        btnOpenPopup2.setOnClickListener(new Button.OnClickListener() {
            final Button btnDismiss = (Button) popupView2.findViewById(R.id.dismiss);

            @Override
            public void onClick(View arg0) {
                popupWindow2.showAsDropDown(finalAnchor2, 50, -30);

                btnDismiss.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        popupWindow2.dismiss();
                    }
                });
            }

        });


        //edo

        final Button btnOpenPopup3 = (Button) findViewById(R.id.openpopup3);
        final View popupView3 = getLayoutInflater().inflate(R.layout.view_info, null);
        anchor = findViewById(R.id.anchor);

        final PopupWindow popupWindow3 = new PopupWindow(
                popupView3,
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);

        finalAnchor1 = anchor;
        final View finalAnchor3 = finalAnchor1;
        btnOpenPopup3.setOnClickListener(new Button.OnClickListener() {
            final Button btnDismiss = (Button) popupView3.findViewById(R.id.dismiss);

            @Override
            public void onClick(View arg0) {
                popupWindow3.showAsDropDown(finalAnchor3, 50, -30);

                btnDismiss.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        popupWindow3.dismiss();
                    }
                });
            }


        });



    }





    public void sendMessage(View view)
    {
        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
        startActivity(intent);
    }
}