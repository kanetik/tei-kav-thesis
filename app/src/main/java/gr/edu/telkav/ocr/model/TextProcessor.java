package gr.edu.telkav.ocr.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gr.edu.telkav.ocr.Configuration;

/**
 * Class processing text into HTML format
 */
public class TextProcessor {
    public TextProcessor() {
    }

    /**
     * Uses regular expressions to find words and linkify them into HTML hyperlink
     *
     * @param originalText text containing key words defined in {@link Configuration.WORDS_MATCHER}
     * @return linkified text
     */

    @NonNull
    public String processText(String originalText) {
        String newText = originalText;
        for (Configuration.WORDS_MATCHER words_matcher : Configuration.WORDS_MATCHER.values()) {
            newText = newText.replaceAll(words_matcher.getRegex(), Configuration.getUrlQuery("$0"));
        }
        return newText;
    }

    public List<String> getMatchedWords(String originalText){
        List<String> foundWords = new ArrayList<>();
        for (Configuration.WORDS_MATCHER words_matcher : Configuration.WORDS_MATCHER.values()) {
            Pattern p = Pattern.compile(words_matcher.getRegex());
            Matcher m = p.matcher(originalText);
            while (m.find()) {
                foundWords.add(m.group());
            }
        }

        return foundWords;
    }

    /**
     * Takes line of text and returns only linkified word that matched definition from {@link Configuration.WORDS_MATCHER}
     *
     * @param originalText single line of text containing words
     * @return single linkified word matched by regex configuration or empty string if no result is found
     */

    @NonNull
    public String findFirstLink(String originalText) {

        for (Configuration.WORDS_MATCHER words_matcher : Configuration.WORDS_MATCHER.values()) {
            Pattern p = Pattern.compile(words_matcher.getRegex());
            Matcher m = p.matcher(originalText);
            if (m.find()) {
                return Configuration.getUrlQuery(m.group(0));
            }
        }

        return "";
    }

}
