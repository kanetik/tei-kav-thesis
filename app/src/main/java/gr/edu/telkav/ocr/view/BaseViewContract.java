package gr.edu.telkav.ocr.view;

import android.content.Context;

/**
 * Interface providing basic context from view to presenter
 *
 * Views like activities should extend this interface to provide
 * methods for presenters to present their data according to
 * Model-View-Presenter design pattern
 */
public interface BaseViewContract {
    Context getContext();
}
