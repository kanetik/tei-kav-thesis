package gr.edu.telkav.ocr;

import android.app.Application;

import timber.log.Timber;

public class App extends Application {
    private static App INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        Timber.plant(new Timber.DebugTree());
    }

    public static App getInstance(){
        return INSTANCE;
    }
}
