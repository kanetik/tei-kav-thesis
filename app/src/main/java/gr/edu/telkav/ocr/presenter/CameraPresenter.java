package gr.edu.telkav.ocr.presenter;

import android.graphics.Bitmap;
import android.renderscript.Allocation;

import com.otaliastudios.cameraview.Frame;

import java.util.List;
import java.util.concurrent.Callable;

import gr.edu.telkav.ocr.model.BitmapTransformer;
import gr.edu.telkav.ocr.model.ImageProcessor;
import gr.edu.telkav.ocr.model.TesseractLoader;
import gr.edu.telkav.ocr.model.TextProcessor;
import gr.edu.telkav.ocr.presenter.base.BasePresenter;
import gr.edu.telkav.ocr.view.screens.continuous.CameraView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Presenter class of continuous camera OCR recognition activity.
 * Responsibility of this presenter is to decode camera preview image
 * and extract recognized text, and creating hyperlink out of it.
 */

public class CameraPresenter extends BasePresenter<CameraView> {

    private final TesseractLoader mTesseractLoader;
    private final ImageProcessor mImageProcessor;
    private final TextProcessor mTextProcessor;
    private final BitmapTransformer mBitmapTransformer;
    private Bitmap mBitmap;

    private String mPreviewText = "";

    public CameraPresenter(TesseractLoader tesseractLoader,
                           ImageProcessor imageProcessor,
                           TextProcessor textProcessor,
                           BitmapTransformer bitmapTransformer) {

        mTesseractLoader = tesseractLoader;
        mImageProcessor = imageProcessor;
        mTextProcessor = textProcessor;
        mBitmapTransformer = bitmapTransformer;
        initTesseract();
    }

    /**
     * Copies training tesseract data in I/O thread
     */
    private void initTesseract() {
        Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mTesseractLoader.initTesseract();
                return true;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    /**
     * Extracts text from bitmap picture and creates hyperlink from recognized words
     *
     * @param data image with text to be recognized
     */
    private void processBitmap(Bitmap data) {
        long time = System.currentTimeMillis();
        Timber.d("decoding started %d", time);
        String extractedText = mImageProcessor.extractTextContinuous(data);
        extractedText = mTextProcessor.findFirstLink(extractedText);
        Timber.d("decoding ended %d", System.currentTimeMillis() - time);
        if (!mPreviewText.contains(extractedText)) {
            mPreviewText += " " + extractedText;
        }
        if (getView() != null) {
            getView().setExtractedText(mPreviewText);
        }
<<<<<<< HEAD
        data.recycle();
=======
>>>>>>> 1b2c1acca9b32b10e1a979b4ee7e466e1be40578
    }

    @Override
    public void onDetachView() {
        super.onDetachView();
        mImageProcessor.closeApi();
    }

    /**
     * @param lockFrame copy of the frame from camera preview
     */
    public void decodePreviewFrame(Frame lockFrame) {
        if(getView() == null) return;

        mBitmap = extractBitmapFromFrame(lockFrame);
        Bitmap finalBitmap = mBitmapTransformer.rotateAndCropBitmap(mBitmap, lockFrame.getRotation());
        getView().setPreviewFrame(mBitmapTransformer.getCropFrame());
        getView().setPreviewBitmap(finalBitmap);
        processBitmap(finalBitmap);
    }

    /**
     * @param lockFrame still frame from camera preview
     * @return Bitmap image of preview frame
     */
    private Bitmap extractBitmapFromFrame(Frame lockFrame){
        if(getView() != null) {
            allocateBitmap(lockFrame.getSize().getWidth(), lockFrame.getSize().getHeight());
            Allocation allocation = mBitmapTransformer.renderScriptNV21ToRGBA888(getView().getContext(),
                    lockFrame.getSize().getWidth(),
                    lockFrame.getSize().getHeight(),
                    lockFrame.getData());
            allocation.copyTo(mBitmap);
        }
        return mBitmap;
    }

    /**
     * @param width = width of preview
     * @param height = height of preview
     */
    private void allocateBitmap(int width, int height) {
        if (mBitmap == null) {
            mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        }
    }

    public List<String> getFoundWords() {
        return mTextProcessor.getMatchedWords(mPreviewText);
    }
}
