package gr.edu.telkav.ocr;

import com.googlecode.tesseract.android.TessBaseAPI;

import gr.edu.telkav.ocr.model.BitmapTransformer;
import gr.edu.telkav.ocr.model.ImageProcessor;
import gr.edu.telkav.ocr.model.TesseractLoader;
import gr.edu.telkav.ocr.model.TextProcessor;
import gr.edu.telkav.ocr.presenter.CameraPresenter;
import gr.edu.telkav.ocr.presenter.OcrPresenter;

/**
 * Factory class constructing presenters and keeping references, so they will survive
 * configuration changes (like screen rotation)
 */

public class ObjectFactory {
    private static ObjectFactory INSTANCE;

    private TesseractLoader mTesseractLoader;
    private ImageProcessor mImageProcessor;
    private TessBaseAPI mTessBaseAPI;
    private TextProcessor mTextProcessor;
    private BitmapTransformer mBitmapTransformer;
    private OcrPresenter mOcrPresenter;
    private CameraPresenter mCameraPresenter;

    public static ObjectFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ObjectFactory();
        }
        return INSTANCE;
    }

    public OcrPresenter getOcrPresenter() {
        if (mOcrPresenter == null) {
            mOcrPresenter = new OcrPresenter(
                    getTesseractLoader(),
                    getImageProcessor(),
                    getTextProcessor());
        }
        return mOcrPresenter;
    }

    private TesseractLoader getTesseractLoader() {
        if (mTesseractLoader == null) {
            mTesseractLoader = new TesseractLoader();
        }
        return mTesseractLoader;
    }

    private ImageProcessor getImageProcessor() {
        if (mImageProcessor == null) {
            mImageProcessor = new ImageProcessor(getTessBaseAPI(),
                    getBitmapTransformer());
        }
        return mImageProcessor;
    }

    private TextProcessor getTextProcessor() {
        if (mTextProcessor == null) {
            mTextProcessor = new TextProcessor();
        }
        return mTextProcessor;
    }

    private TessBaseAPI getTessBaseAPI() {
        if (mTessBaseAPI == null) {
            mTessBaseAPI = new TessBaseAPI();
        }
        return mTessBaseAPI;
    }

    private BitmapTransformer getBitmapTransformer() {
        if (mBitmapTransformer == null) {
            mBitmapTransformer = new BitmapTransformer();
        }
        return mBitmapTransformer;
    }

    public CameraPresenter getCameraPresenter() {
        if (mCameraPresenter == null) {
            mCameraPresenter = new CameraPresenter(
                    getTesseractLoader(),
                    getImageProcessor(),
                    getTextProcessor(),
                    getBitmapTransformer());
        }
        return mCameraPresenter;
    }
}
