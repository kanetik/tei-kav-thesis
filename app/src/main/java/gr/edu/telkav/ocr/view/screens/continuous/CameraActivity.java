package gr.edu.telkav.ocr.view.screens.continuous;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.otaliastudios.cameraview.CameraLogger;
import com.otaliastudios.cameraview.CameraView;

import gr.edu.telkav.ocr.ObjectFactory;
import gr.edu.telkav.ocr.R;
import gr.edu.telkav.ocr.presenter.CameraPresenter;
import gr.edu.telkav.ocr.view.BaseActivity;
import gr.edu.telkav.ocr.view.camera2.Control;
import gr.edu.telkav.ocr.view.camera2.ViewfinderView;


public class CameraActivity extends BaseActivity<CameraPresenter>
        implements View.OnClickListener, ControlView.Callback,
        gr.edu.telkav.ocr.view.screens.continuous.CameraView {

    private CameraView mCamera;
    private ViewGroup mControlPanel;
    private ImageView mImageView;
    private ContinuousFrameProcessor mFrameProcessor;
    private TextView mTextView;
    private ViewfinderView mViewfinderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        setContentView(R.layout.activity_camera2);
        CameraLogger.setLogLevel(CameraLogger.LEVEL_VERBOSE);
        initViews();
        initControlPanel();
    }

    private void initViews() {
        findViewById(R.id.edit).setOnClickListener(this);
        mTextView = findViewById(R.id.textPreview);
        mTextView.setClickable(true);
        mTextView.setMovementMethod(LinkMovementMethod.getInstance());
        mViewfinderView = findViewById(R.id.viewFinder);

        mImageView = findViewById(R.id.preview);
        mCamera = findViewById(R.id.camera);
        mCamera.setCropOutput(true);
        mFrameProcessor = new ContinuousFrameProcessor(getPresenter());
    }

    private void initControlPanel() {
        mControlPanel = findViewById(R.id.controls);
        ViewGroup group = (ViewGroup) mControlPanel.getChildAt(0);
        Control[] controls = Control.values();
        for (Control control : controls) {
            ControlView view = new ControlView(this, control, this);
            group.addView(view, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        mControlPanel.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                BottomSheetBehavior b = BottomSheetBehavior.from(mControlPanel);
                b.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });
    }

    @Override
    protected CameraPresenter createPresenter() {
        return ObjectFactory.getInstance().getCameraPresenter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.share, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.ab_share){
            PopupMenu menu = new PopupMenu(this, findViewById(R.id.ab_share));
            for (String word : getPresenter().getFoundWords()) {
                menu.getMenu().add(word);
            }
            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    setShareIntent(item.getTitle().toString());
                    return true;
                }
            });
            menu.show();
        }
        return true;
    }

    // Call to update the share intent
    private void setShareIntent(String shareWord) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareWord);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mCamera.addFrameProcessor(mFrameProcessor);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCamera.removeFrameProcessor(mFrameProcessor);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit:
                edit();
                break;
        }
    }

    private void edit() {
        BottomSheetBehavior b = BottomSheetBehavior.from(mControlPanel);
        b.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onBackPressed() {
        BottomSheetBehavior b = BottomSheetBehavior.from(mControlPanel);
        if (b.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            b.setState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCamera.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCamera.start();
    }

    //region Boilerplate

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean valid = true;
        for (int grantResult : grantResults) {
            valid = valid && grantResult == PackageManager.PERMISSION_GRANTED;
        }
        if (valid && !mCamera.isStarted()) {
            mCamera.start();
        }
    }

    @Override
    public boolean onValueChanged(Control control, Object value, String name) {
        if (!mCamera.isHardwareAccelerated() && (control == Control.WIDTH || control == Control.HEIGHT)) {
            if ((Integer) value > 0) {
                message("This device does not support hardware acceleration. " +
                        "In this case you can not change width or height. " +
                        "The view will act as WRAP_CONTENT by default.", true);
                return false;
            }
        }
        control.applyValue(mCamera, value);
        BottomSheetBehavior b = BottomSheetBehavior.from(mControlPanel);
        b.setState(BottomSheetBehavior.STATE_HIDDEN);
        message("Changed " + control.getName() + " to " + name, false);
        return true;
    }

    private void message(String content, boolean important) {
        int length = important ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
        Toast.makeText(this, content, length).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCamera.destroy();
    }

    @Override
    public void setExtractedText(final String extractedText) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTextView.setText(Html.fromHtml(extractedText));
               invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void setPreviewBitmap(final Bitmap bitmap) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mImageView.setImageBitmap(bitmap);
            }
        });
    }

    @Override
    public void setPreviewFrame(final Rect previewFrame) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mViewfinderView.setPreviewFrame(previewFrame);
                mViewfinderView.drawViewfinder();
            }
        });
    }
}
