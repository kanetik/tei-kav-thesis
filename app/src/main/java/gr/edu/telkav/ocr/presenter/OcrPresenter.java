package gr.edu.telkav.ocr.presenter;

import android.graphics.Bitmap;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import gr.edu.telkav.ocr.model.ImageProcessor;
import gr.edu.telkav.ocr.model.TesseractLoader;
import gr.edu.telkav.ocr.model.TextProcessor;
import gr.edu.telkav.ocr.presenter.base.BasePresenter;
import gr.edu.telkav.ocr.view.screens.ocr.OcrView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Presenter class for activity decoding image into text with hyperlinks
 * created from recognized words defined in {@link gr.edu.telkav.ocr.Configuration.WORDS_MATCHER}
 */

public class OcrPresenter extends BasePresenter<OcrView> {
    private String mExtractedText;
    private Uri mImageUri;
    private List<String> mFoundWords;

    private final TesseractLoader mTesseractLoader;
    private final ImageProcessor mImageProcessor;
    private final TextProcessor mTextProcessor;

    public OcrPresenter(TesseractLoader tesseractLoader,
                        ImageProcessor imageProcessor,
                        TextProcessor textProcessor) {
        mTesseractLoader = tesseractLoader;
        mImageProcessor = imageProcessor;
        mTextProcessor = textProcessor;
        initTesseract();
    }

    @NonNull
    public List<String> getFoundWords() {
        return mFoundWords == null ? new ArrayList<String>() : mFoundWords;
    }

    /**
     * Copies training tesseract data in I/O thread
     */
    private void initTesseract() {
        Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mTesseractLoader.initTesseract();
                return true;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    /**
     * Processes image into greyscale and extracts text from it.
     * Afterwards linkifies recognized words
     *
     * @param resultUri URI of picture to be processed by OCR engine
     */
    public void startOCR(Uri resultUri) {
        mImageUri = resultUri;
        mImageProcessor.postProcessImage(resultUri).subscribe(
                new Observer<Bitmap>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        if (getView() != null) getView().showLoading(true);
                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        mExtractedText = mImageProcessor.extractText(bitmap);
                        mFoundWords = mTextProcessor.getMatchedWords(mExtractedText);
                        mExtractedText = mTextProcessor.processText(mExtractedText);
                        if (getView() != null) {
                            getView().showLoading(false);
                            getView().submitText(mExtractedText);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (getView() != null) {
                            getView().showLoading(false);
                            getView().submitText("error");
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                }
        );
    }

    /**
     * After presenter is attached to activity, make it display data decoded by OCR
     */
    @Override
    public void onAttachView() {
        if (mExtractedText != null && mImageUri != null) {
            if (getView() != null) {
                getView().submitText(mExtractedText);
                getView().loadPreview(mImageUri);
            }
        }

    }
}
