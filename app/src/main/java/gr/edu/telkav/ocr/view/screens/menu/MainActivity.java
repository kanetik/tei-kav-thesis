package gr.edu.telkav.ocr.view.screens.menu;

import android.content.Intent;
import android.os.Bundle;
<<<<<<< HEAD
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import gr.edu.telkav.ocr.HomeActivity;
=======
import android.view.View;
import android.widget.TextView;

>>>>>>> 1b2c1acca9b32b10e1a979b4ee7e466e1be40578
import gr.edu.telkav.ocr.R;
import gr.edu.telkav.ocr.presenter.MenuPresenter;
import gr.edu.telkav.ocr.view.BaseActivity;
import gr.edu.telkav.ocr.view.screens.continuous.CameraActivity;
import gr.edu.telkav.ocr.view.screens.ocr.OcrActivity;

public class MainActivity extends BaseActivity<MenuPresenter> implements MenuView {
<<<<<<< HEAD


    private static int SPLASH_TIME_OUT = 500;



=======
>>>>>>> 1b2c1acca9b32b10e1a979b4ee7e466e1be40578
    TextView mCaptureBtn;
    TextView mContinuousBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        mCaptureBtn = findViewById(R.id.textCapture);
        mContinuousBtn = findViewById(R.id.textContinuous);

        mCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, OcrActivity.class));
            }
        });

        mContinuousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CameraActivity.class));
            }
        });
    }

    @Override
    protected MenuPresenter createPresenter() {
        return new MenuPresenter();
    }
}
