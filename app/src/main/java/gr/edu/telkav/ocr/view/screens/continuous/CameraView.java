package gr.edu.telkav.ocr.view.screens.continuous;

import android.graphics.Bitmap;
import android.graphics.Rect;

import gr.edu.telkav.ocr.view.BaseViewContract;

public interface CameraView extends BaseViewContract {

    void setExtractedText(String extractedText);

    void setPreviewBitmap(Bitmap bitmap);

    void setPreviewFrame(Rect frame);
}
