package gr.edu.telkav.ocr;

public class Configuration {
    public static final String LANGUAGE = "ell";
    public static final String DATA_PATH = App.getInstance().getFilesDir().toString() + "/TesseractSample/";

    public static String getUrlQuery(String query) {
        return "<a href='https://www.google.gr/search?q=" + query + "'>" + query + "</a>";
    }


    public enum WORDS_MATCHER {
        V_123_1234("(ν\\.\\s*\\n*)[0-9Ο]{1,4}(\\/)[0-9Ο]{4}"),
        FEK_1_12_12_1234_tA("(\\(ΦΕΚ\\s*\\n*)[0-9Ο]{1,4}(\\/)[0-9Ο]{1,2}(-)[0-9Ο]{1,2}(-)[0-9Ο]{4}(\\/)(τ\\.Α([΄'])\\))");

        private String mRegex;

        WORDS_MATCHER(String regex) {
            mRegex = regex;
        }

        public String getRegex() {
            return mRegex;
        }
    }
}
